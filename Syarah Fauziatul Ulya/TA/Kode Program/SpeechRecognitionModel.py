# In[1]:
import librosa #membaca lagu
import librosa.feature
import librosa.display
import glob #mengambil data audio
import numpy as np #array
import matplotlib.pyplot as plt #membuat plot
from keras.models import Sequential #keras ~ tensorflow
from keras.layers import Dense, Activation
from keras.utils.np_utils import to_categorical

# In[2]:
def display_mfcc(audio):
    #fungsi untuk menampilkan vektorisasi suara 
    y, _ = librosa.load(audio)
    #variabel y membaca variabel audio dari perintah library load audio
    mfcc = librosa.feature.mfcc(y) 
    #variabel mfcc untuk variabel y, mengubah suara menjadi vektor 
    
    plt.figure(figsize=(10, 4))
    #memplot gambar dengan ukuran 10x4
    librosa.display.specshow(mfcc, x_axis='time', y_axis='mel')
    #menampilkan spektogram
    plt.colorbar()
    #menambahkan colorbar
    plt.title(audio)
    #memberi judul audio
    plt.tight_layout()
    #memberi label pada sumbu di grafik
    plt.show()
    #menampilkan hasil plot dalam bentuk spektogram
    
# In[3]: Memanggil fungsi display_mfcc untuk menampilkan hasil plot
# dari suara menjadi vektor dalam bentuk spektogram
display_mfcc('E:/Kode Program/Dataset/speaker/19/19-198-0001.au')

# In[4]:
display_mfcc('E:/Kode Program/Dataset/speaker/26/26-495-0000.au')

# In[5]:
display_mfcc('E:/Kode Program/Dataset/speaker/27/27-123349-0000.au')

# In[6]:
def extract_features_audio(f):
    #membuat fungsi dengan inputan f
    y, _ = librosa.load(f) 
    #variabel y untuk meload inputan f
    
    # get Mel-frequency cepstral coefficients
    mfcc = librosa.feature.mfcc(y)
    #variabel mfcc untuk membuat feature dari variabel y
    
    # normalize values between -1,1 (divide by max)
    mfcc /= np.amax(np.absolute(mfcc))
    #membuat normalisasi nilai antara -1 dan 1
    
    return np.ndarray.flatten(mfcc)[:25000] 
    #diambil 25000 row pertama berdasarkan durasi suara

# In[7]:
def generate_features_and_labels():
    all_features = [] 
    #variabel yang berisi array kosong
    all_labels = []
    
    speakers= ['19', '26', '27', '32', '39', '40', '60', '78', '83', '87', '89', '103', '118'] 
    #variabel speaker berisi nama-nama folder dari dataset yang digunakan
    for speaker in speakers: 
    #looping dari folder speaker
        
        sound_files = glob.glob('E:/Kode Program/Dataset/speaker/'+speaker+'/*.au')
        #membuat atribut sound_files yang berisi perintah looping per folder
        print('Processing %d audio in %s speaker...' % (len(sound_files), speaker))
        #menampilkan jumlah audio yang dieksekusi
        for f in sound_files:
        #membuat perintah fungsi dari sound_files
            
            features = extract_features_audio(f)
            #variabel features untuk memanggil fungsi extract_feature_audio,
            #f sebagai inputan didalam soundfile
            all_features.append(features)
            #vektoriksasi, memasukkan semua fitur
            #dengan perintah append ke dalam all_features
            all_labels.append(speaker)
            #append seperti stack=ditumpuk
        
    # convert labels to one-hot encoding
    label_uniq_ids, label_row_ids = np.unique(all_labels, return_inverse=True)
    label_row_ids = label_row_ids.astype(np.int32, copy=False)
    onehot_labels = to_categorical(label_row_ids, len(label_uniq_ids))
    return np.stack(all_features), onehot_labels
# In[8]: passing parameter dari fitur ekstraksi menggunakan MFCC
features, labels = generate_features_and_labels()
# In[9]:
print(np.shape(features))
print(np.shape(labels))
# In[10]:
training_split = 0.8
# In[11]:
# last column has speak, turn it into unique ids
alldata = np.column_stack((features, labels))
# In[12]:
np.random.shuffle(alldata)
splitidx = int(len(alldata) * training_split)
train, test = alldata[:splitidx,:], alldata[splitidx:,:]
# In[13]:
print(np.shape(train))
print(np.shape(test))
# In[14]:
train_input = train[:,:-13] 
#kolom 13 terakhir tidak diikutsertakan
train_labels = train[:,-13:]
# In[15]:
test_input = test[:,:-13]
test_labels = test[:,-13:]
# In[16]:
print(np.shape(train_input))
print(np.shape(train_labels))
# In[17]: membuat model sequential dari keras
model = Sequential([
        Dense(85, input_dim=np.shape(train_input)[1]),
        Activation('relu'), #relu=rectifier linier unit, fungsinya untuk mencari nilai maksimum yang akan dipilih
        Dense(13),
        Activation('softmax'), 
        ])
# In[18]: compile dengan algoritma adam
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
print(model.summary()) #munculin summary
# In[19]: pelatihan dari dari training
model.fit(train_input, train_labels, epochs=13, batch_size=32, validation_split=0.2) #cek nilai skor cross validation
# In[20]: eavluasi
loss, acc = model.evaluate(test_input, test_labels, batch_size=32)
print("Done!")
print("Loss: %.4f, accuracy: %.4f" % (loss, acc))
# In[21]: save model
model.save("E:/Kode Program/features_extraction.model")
# In[22]: Predict
model.predict(test_input[:1]
